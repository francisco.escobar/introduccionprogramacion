# Libros y sitios complementarios

### Documentación Oficial

#### [Sitio oficial Python](https://www.python.org/)

En este sitio podras encontrar información asociada a Python, descargas asociadas, etc.

#### [Sitio oficial pypi](https://pypi.org/)

En este sitio encontraras las librerias públicas en python que podrias utilizar en tus proyectos.

#### [Documentación Oficial](https://docs.python.org/3/)

En este sitio encontrarás la documentación oficial del lenguaje de programación python en sus distintas versiones (por defecto siempre se muestra la última oficial).

## Sitios

#### [El libro de Python](https://ellibrodepython.com/)
#### [Algoritmos de Programación con Python](https://uniwebsidad.com/libros/algoritmos-python)
#### [Python para Principantes](https://uniwebsidad.com/libros/python)
#### [RealPython](https://realpython.com/)

## Libros

#### [Python in a Nutshell](https://www.oreilly.com/library/view/python-in-a/9781491913833/)
#### [Python Cookbook](https://www.amazon.com/Python-Cookbook-Third-David-Beazley/dp/1449340377/ref=pd_bxgy_img_sccl_2/145-3309356-7423209?pd_rd_w=Dl1Kc&content-id=amzn1.sym.7757a8b5-874e-4a67-9d85-54ed32f01737&pf_rd_p=7757a8b5-874e-4a67-9d85-54ed32f01737&pf_rd_r=EQFX6E1DJNTX40C658XT&pd_rd_wg=JTzNT&pd_rd_r=680fd4d1-73b8-40b8-8938-b7ee7711eb36&pd_rd_i=1449340377&psc=1)
#### [Learn Python 3 The Hard Way](https://www.amazon.com/gp/product/0134692888/ref=as_li_qf_sp_asin_il_tl?ie=UTF8&tag=lepythhawa-20&camp=1789&creative=9325&linkCode=as2&creativeASIN=0134692888&linkId=9584c3f39e972b770bd16b38594294cf)
#### [Python Crash course](https://nostarch.com/pythoncrashcourse2e)


## Cursos

#### [Programming for Everybody (Getting Started with Python)](https://www.classcentral.com/course/python-4319)

- Chapter One - Why we Program?
- Chapter One: Why We Program (continued)
- Chapter Two: Variables and Expressions
- Chapter Three: Conditional Code
- Chapter Four: Functions
- Chapter Five: Loops and Iteration

#### [Object Oriented Principles](https://www.classcentral.com/course/object-oriented-principles-8884)

- Using objects
- Creating classes
- Extending classes
- Extending your game and your OOP knowledge

#### [Introduction to Computer Science and Programming Using Python](https://www.classcentral.com/course/edx-introduction-to-computer-science-and-programming-using-python-1341)

- Introduction to Computer Science
- Programming Using Python
- Introduction to Computational Thinking
- Introduction to Data Science


## Videos

#### [Python Tutorial - Python Full Course for Beginners](https://www.youtube.com/watch?v=_uQrJ0TkZlc&ab_channel=ProgrammingwithMosh)
#### [Curso Python para Principiantes](https://www.youtube.com/watch?v=chPhlsHoEPo&t=4513s&ab_channel=Fazt)
#### [Python for Everybody - Full University Python Course](https://www.youtube.com/watch?v=8DvywoWv6fI&ab_channel=freeCodeCamp.org)
#### [Aprende Python - Curso completo para priincipiantes](https://www.youtube.com/watch?v=rfscVS0vtbw&ab_channel=freeCodeCamp.org)
#### [Intermediate Python Programming Course](https://www.youtube.com/watch?v=HGOBQPFzWKo&ab_channel=freeCodeCamp.org)



## Juegos

#### [CODEWARS](https://www.codewars.com/)
#### [Coding Game](https://www.codingame.com/)


