# Metodos/Funciones

Esta nueva técnica, llamada diseño modular, te aportará dos ventajas: por una parte, completará y ampliará al diseño descendente como método de resolución de problemas algorítmicos; por otra, permitirá proteger la estructura de la información asociada a un determinado problema, limitando las operaciones que puedan actuar sobre ellas.

## Conceptos basicos
### Verificabilidad
Un software es verificable si sus propiedades pueden ser verificadas fácilmente. Por ejemplo, la correctitud o la performance de un software son propiedades que interesa verificar. El diseño modular, prácticas de codificación disciplinadas, y la utilización de lenguajes de programación adecuados contribuyen a la verificabilidad de un software.

La verificabilidad es en general una cualidad interna pero a veces también puede ser externa, por ejemplo, en muchas aplicaciones de seguridad crítica, el cliente requiere la verificación de ciertas propiedades.

### Legibilidad
Es posible tener dos versiones diferentes de un programa de software que funcionen exactamente de la misma manera, y que tengan el mismo nivel interno de diseño y construcción desde una perspectiva técnica, pero los cuales son vastamente diferentes en su legibilidad humana. Si un sistema de software no es legible, es difícil (o imposible) depurarlo, modificarlo, ampliarlo, escalarlo, etc. Hay otros dos aspectos concernientes a la legibilidad del software: la claridad que está construida dentro del código, y los comentarios que acompañan al código. La primera incluye nombres significativos para las variables y constantes, buen uso del espaciado y la indentación, estructuras de control transparentes, y rutas de ejecución normales y directas. La práctica de comentar bien el código fuerza a incluir comentarios que eduquen al próximo programador sobre los tópicos que no pueden ser inferidos del código mismo.

### Reusabilidad

La noción de módulo permite que programas que traten las mismas estructuras de información reutilicen las funciones empleadas en otros programas. De esta forma, el desarrollo de un programa puede llegar a ser una simple combinación de funciones ya definidos donde estos están relacionados de una manera particular.

### Modificabilidad

Un software es modificable si permite la corrección de sus defectos con una carga limitada de trabajo. En otros campos de la ingeniería puede ser más barato cambiar un producto entero o una buena parte del mismo que repararlo, por ejemplo televisores, y una técnica muy utilizada para lograr modificabilidad es usar partes estándares que puedan ser cambiadas fácilmente. Sin embargo, en el software las partes no se deterioran, y aunque el uso de partes estándares puede reducir el costo de producción del software, el concepto de partes reemplazables pareciera no aplicar a la modificabilidad del software. Otra diferencia es que el costo del software está determinado, no por partes tangibles sino por actividades humanas de diseño. Un producto de software consistente en módulos bien diseñados es más fácil de analizar y reparar que uno monolítico, sin embargo, el solo aumento del número de módulos no hace que un producto sea más modificable. Una modularización adecuada con definición adecuada de interfaces que reduzcan la necesidad de conexiones entre los módulos promueve la modificabilidad ya que permite que los errores estén ubicados en unos pocos módulos, facilitando la localización y eliminación de los mismos.

#### Ejemplo
Consideremos un programa para calcular las remuneraciones de un empleado. En un primer análisis podemos diferenciar diversas tareas casi independientes:

1. El acceso a los datos personales y profesionales de un empleado: por lo general estos datos, se encuentran almacenados en archivos o en bases de datos (colección de archivos administrados por un sistema). Los datos personales están asociados a su Rut, nombre, dirección, entre otros; y los datos profesionales están asociados al título profesional, el grado que le asigna la empresa, los años de experiencia; que generalmente determinan su sueldo base.

2. Calcular los bonos del empleado: hay veces en que un empleado dispone de bonos especiales, como por ejemplo: bonos por cargas familiares, bonos de producción, bonos profesionales, bonos por zona de trabajo, bonos por aguinaldo (fiestas patrias y navidad), entre otros.

3. Calcular los descuentos: es normal que al sueldo base de un empleado se le apliquen ciertos descuentos, como por ejemplo: descuentos por adelantos que solicitó el empleado, descuento por salud, descuento por AFP, descuentos por pagos, etc.

4. Calcular pagos por horas extras de trabajo: cuando un empleado trabaja horas extras, la organización que lo contrata debe otorgarle un pago por ello. Normalmente la cantidad que se l pague está asociado a las políticas que aplica la empresa. Por ejemplo, una política de la empresa es que se pagará $6.000 pesos por hora extra trabajada si el empleado es un técnico.

5. Calcular remuneración: Esta tarea consiste en calcular la remuneración de un empleado en particular para un determinado mes del año. Esta tarea debe considerar la información que le proporcionen las otras actividades descritas en los párrafos anteriores.

6. Imprimir la liquidación de sueldo: consiste en imprimir todos los datos que tiene que ver con el empleado en particular, su sueldo base, bonos y descuentos; en algún formato que determine la empresa.


Es fácil observar que lo más razonable es resolver estos problemas por separado, estableciendo los canales de comunicación apropiados entre todos ámbitos de trabajo para combinar estas soluciones y lograr el objetivo final.

### Módulo

Un módulo es una colección de declaraciones, en principio ocultas respecto a toda acción o declaración ajena al propio módulo, es decir, definidas en un ámbito de visibilidad cerrado.

Emplearemos el concepto de módulo para materializar cada una de las unidades en las que se tendrá que descomponer toda tarea de programación mínimamente importante, de modo que una vez conectados convenientemente estos módulos resulte una estructura que resuelva el problema en cuestión.

### Diseño modular

El uso de módulos se acompaña de una metodología de desarrollo de problemas basada en la descomposición de éstos en unidades independientes y la resolución separada de los subproblemas que surgen. Denominamos "diseño modular" a tal estrategia.

Algunas ventajas que podemos asociar al diseño modular son:

- Permite resolver el problema por partes. Esto nos obliga a determinar la estructura de cada parte o módulo. Cada módulo del problema será resuelto por una función.
- Permite el trabajo en equipo. Ya que cada módulo o función es desarrollada por un determinadoprogramador. La solución completa estará determinada por la integración de todas las funciones del programa.
- Permite la reutilización. Esto se debe a que una función puede ser utilizada por varios programas. Esto permite acelerar el tiempo de desarrollo del software ya que podemos utilizar código fuente que ya está probado y funcionando.

El fundamento de un buen diseño modular consiste en contemplar nuestros futuros algoritmos como una jerarquía de módulos perfectamente comunicados entre sí, donde cada uno de ellos cuenta con un objetivo diferenciado, como si fueran piezas de una máquina que pudiesen ser utilizadas para construir otras máquinas.

![diseño](diseñoM.png)

## Subprogramas

### Subprogramas

Un subprograma es una colección de sentencias que posee varias características:

- Posee su propio nombre: El conjunto de sentencias considerado como subprograma debe disponer de un identificador exclusivo. Este identificador bastará para que el compilador o traductor sepa a qué subprograma se alude cuando un identificador aparezca en una llamada.
- Se puede invocar como un todo: Se puede solicitar la ejecución de esa colección de sentencias mencionando el nombre acordado para ellas. En la solicitud de ejecución, denominada llamada a la función, se proporcionan a la misma ciertos valores denominados argumentos. Cuando finaliza la ejecución de las sentencias, el control de flujo del programa vuelve a la primera sentencia posterior a la llamada al subprograma.
- Puede recibir y devolver información: El subprograma recibe, cuando es invocada, una colección de valores denominados argumentos. Y las variables que reciben los datos desde los argumentos se denominan parámetros formales.
- Dispone de sus propios espacios de memoria: Los subprogramas permiten declarar variables en su interior. Estas variables se denominan variables locales, y se caracterizan porque sólo se puede acceder a ellas desde el subprograma que las crea.

![funcion](funcion.png)

- **NombreFuncion**: Es el nombre que identifica la función para ser llamada desde otra función.
- **Parametros Formales**: Son las variables que reciben datos desde la función que realiza la llamda.
- **Return**: Permite devolver el resultado de la función.
- **Variables Locales**: Son las variables que requiere la función para resolver el problema.

### Argumento
Son los datos que enviaremos a la función cuando esta es invocada desde otra función.

### Parámetro Formal
Los parámetros son variables que permiten a la función recibir datos cuando ésta es invocada (o llamada). Existen tres reglas para crear los parámetros de una función.

- La cantidad de Parámetros debe ser la misma que los Argumentos: Se requiere crear un parámetro formal por cada argumento que se cree en la llamada a la función. Por ejemplo, si disponemos de 2 argumentos en la llamada, quiere decir que dos son los datos que enviaremos a la función por medio de sus parámetros.
- Argumentos y Parámetros deben ser de tipo compatible: Cada parámetro recibirá un dato específico que dependerá del dato que tiene almacenado el argumento, por tanto el tipo de dato que se defina para el parámetro formal debe permitir almacenar el dato que proviene desde el argumento.
- El traspaso de datos desde los Argumentos hacia los Parámetros debe ser en orden: Cada argumento entrega los datos a sus respectivos parámetros de manera ordenada, uno por uno, por tanto, los parámetros deben ser declarados en el miso orden que se especifican los argumentos al momento de llamar a la función.

### Variable local
Son las variables que se declaran al interior de una función. Estas variables sólo pueden ser utilizadas por la función que las crea, y existirán cuando la función es invocada.

### Variable Global
Son variables que se declaran fuera de cualquier función del programa, normalmente después de la sección de Librerías. Estas variables pueden ser utilizadas por cualquiera de las funciones escritas en el programa ya que existen mientras se ejecute el programa (desde que se ejecuta), lo cual es cómodo y supone un grave riesgo. Por lo tanto no dependen de la ejecución de una función específica.

***Nota***:
*Debemos considerar que el uso de variables globales puede traer problemas para realizar modificaciones en las funciones. Por tanto limitaremos el usode éstas sólo para casos excepcionales.
En efecto cualquier función podría hacer uso de una variable global considerando que era local, con resultados.*

## Declaración
### Encabezado de la función

El Encabezado de la función debe declararse en la primera línea de la función. Consta de 2 bloques de información:
```python
def nombre_de_la_función (parámetros formales):
```

- El nombre de la función es un identificador válido en Java. Los nombres de funciones no deben ser palabras reservadas de lenguaje de programación (por ejemplo, read, nextInt, sqrt, int). Para hacer la llamada a una función se escribe el nombre de la función y a continuación una pareja de paréntesis. Si la función permite argumentos, los parámetros se colocan dentro de los paréntesis (pero sin escribir los nombres de las variables).
- Los parámetros de entrada o parámetros formales de la función son una colección de declaraciones de tipos de datos, si es que la función admite argumentos.

### Cuerpo de la Función

El cuerpo de la función se define a continuación del encabezado de la función con una colección de sentencias entre llaves. La función finaliza su ejecución cuando el flujo de control alcanza la sentencia return. Si la función es de tipo void, se puede omitir la sentencia return; entonces la ejecución finaliza cuando se haya ejecutado la última sentencia de la función.

El código siguiente muestra de manera general el cuerpo de una función. Comúnmente llamamos a este hecho "implementación de la función".

```python
def nombre_de_la_función (parámetros formales):
   declaraciones de variables locales
   sentencia1
   sentencia2
   ...
   sentenciaN
   return expresión_de_tipo_compatible_con_el_tipo_proporcionado


```

## Ejemplos de Declaración Funciones

### Sin parámetros ni resultados
Se pueden crear funciones que no admiten parámetros ni proporcionan resultados. El encabezado de este tipo de funciones es el siguiente:

```python
def nombre_de_la_función ():
```
cuerpo

```python
    variables locales
    instrucción_1
    instrucción_2
    ...
    instrucción_N

```

### Funciones con parámetros y sin resultados
El encabezado de una función con parámetros y sin resultados es como sigue:
```python
def nombre_funcion (tipo1, tipo2,...,tipoN):
```
Cuerpo
```python

    variables locales
    instrucción 1
    instrucción 2
    ...
    instrucción n

```
Esta función puede comunicarse con el resto del programa por medio de los argumentos (arg1, arg2, ..., argN). De esta manera no es necesario recurrir a variables globales; es suficiente con los argumentos, que permiten efectuar un paso de información en ambos sentidos (de la función A que llama a la función B, y de la función B que es llamada por la función A).

### Funciones sin parámetros y con resultados

Las funciones que proporcionan un resultado y carecen de parámetros tienen un encabezado como el que sigue:

```python
def nombre_funcion ():
```
Cuerpo

```python

    instrucción 1
    instruccón 2
    instrucción 3
    ...
    instrucción N

    return resultado 

```

Las funciones que retornan un resultado se pueden llamar de varias formas:

- Desde un "print" para imprimir el resultado después de ejecutar la función
```python
print(llamada_función ())
```
- Almacenando el resultado retornado en otra variable
```python
variable = llamada_función()
```
- Dentro de una condición lógica
```java
if llamada_función() > 0 :
```

***Nota***: Tenga en cuenta que una función es capaz de devolver un solo resultado.
Esto quiere decir, que no se admiten retornos de la forma:
``` return r1, r2, rN  ```

### Funciones con parámetros y resultados

Son las de uso más frecuente. Su prototipo es como sigue:

```Python
def nombreFunción (parámetros formales)

```
Cuerpo

```java
def nombreFunción (parámetros formales):
   variables locales
   instrucción_1
   instrucción_2
   instrucción_3
   ...
   instrucción_N
   return valor_compatible_con_tipo;

```

Recuerde que las funciones que retornan un resultado se pueden llamar de varias formas:

- Desde un ```print``` para imprimir el resultado después de ejecutar la función
```python
print(llamada_función (parámetros))
```
- Almacenando el resultado retornado en otra variable
```python
Otra_var = llamada_función (parámetros)
```
- Dentro de una condición lógica
```python
if llamada_función (parámetros) > 0 :
```


## Paso por
### Paso por Valor
El paso de información por valor consiste en evaluar los argumentos y traspasar el valor a los parámetros formales. Luego de ello, se ejecuta el código de la función. Esto implica que ha ocurrido un paso de información desde el exterior.

#### Ejemplo
![ppvalor](ppvalor.png)

```python
def mostrar(num1, num2):
    print("num1: ", num1)
    print("num2: ", num2)
    num1 = 2;
    num2 = 5;
    print("num1: ", num1)
    print("num2: ", num2)

i = 10  #variable local
j = 12  #variable local
print("varI: ", i)
print("varJ: ", j)
mostrar(i, j)
print("varI: ", i)
print("varJ: ", j)
```

```python
# Salida
varI: 10    # Salida desde dentro de la funcion principal
varJ: 12    # Salida desde dentro de la funcion principal
num1: 10    # Salida desde dentro de la funcion mostrar, valor de variable i
num2: 12    # Salida desde dentro de la funcion mostrar, valor de variable j
num1: 2     # Salida desde dentro de la funcion mostrar, luego de cambio de valor;
num2: 5     # Salida desde dentro de la funcion mostrar, luego de cambio de valor;
varI: 10    # Salida desde dentro de la funcion principal, el valor no cambia luego de la funcion.
varJ: 12    # Salida desde dentro de la funcion principal, el valor no cambia luego de la funcion.

```

### Paso por "Referencia"
Vimos en la sección anterior que no es posible cambiar el valor de una variable externa desde una función.
Sin embargo, existe un caso especial cuando se tiene una lista, diccionario, conjuntos, etc. El cual funciona de la siguiente manera:

#### Ejemplo
![ppReferencia](ppreferencia.png)

```python
import random

def llenar(lista):
    for x in range(0,10):
        lista.append(random.randint(0,100))
    
numeros = []
print(numeros)
llenar(numeros)
print(numeros)

```

```python
//salida
    [] # El arreglo es creado y visualizado por la funcion print
    # Se llena el arreglo dentro de la función llenar.
    [96][16][84][7][78][23][55][56][80][26] # El arreglo es visualizado luego del cambio efectuado
```

## Ejemplo de uso de funciones de python

Python posee una una serie de funciones que estan disponibles sin ser necesaria ninguna libreria.

| | |Funciones Integradas| | |
|-|-|-|-|-|
|```abs()```        | ```delattr()```   | ``` hash()```     | ```memoryview()```| ```set()```|
|```all()```        | ```dict()```      | ```help()```      | ```min()```       | ```setattr()```|
|```any()```        | ```dir()```       | ```hex()```       | ```next()```      | ```slice()```|
|```ascii()```      | ```divmod()```    | ```id()```        | ```object()```    | ```sorted()```|
|```bin()```        | ```enumerate()``` | ```input()```     | ```oct()```       | ```staticmethod()```|
|```bool()```       | ```eval()```      | ```int()```       | ```open()```      | ```str()```|
|```breakpoint()``` | ```exec()```      | ```isinstance()```| ```ord()```       | ```sum()```|
|```bytearray()```  | ```filter()```    | ```ssubclass()``` | ```pow()```       | ```super()```|
|```bytes()```      | ```float()```     | ```iter()```      | ```print()```     | ```tuple()```|
|```callable()```   | ```format()```    | ```len()```       | ```property()```  | ```type()```|
|```chr()```        | ```frozenset()``` | ```list()```      | ```range()```     | ```vars()```|
|```classmethod()```| ```getattr()```   | ```locals()```    | ```repr()```      | ```zip()```|
|```compile()```    | ```globals()```   | ```map()```       | ```reversed()```  | ```__import__()```|
|```complex()```    | ```hasattr()```   | ```max()```       | ```round()```     ||



### Demo funciones matematicas 
```python

num1 = pow(3,5)   #llamada a la función pow con 3 y 5 como argumento y retorna la 5° potencia de 3
num2 = abs(-5)    #llamada a la función abs con -5 como argumento y retorna el valor absoluto de -5

```

### Demo de funciones para estructuras de datos

```python

num0 = sum([3,4])           #llamada a la función sum con una lista como argumento y retorna la suma de los elementos

cadena0 = "hola, mundo"
largoString = len(cadena0)  #llamada a la función len con un string como argumento y retorna el largo de la cadena

lista0 = [3, 4, 5, 1, 2]
tupla0 = tuple(lista0)      #llamada a la función tuple con una lista como argumento y retorna una tupla con los elementos de la lista
```
