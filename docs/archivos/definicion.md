# Manejo de archivos

Un complemento importante a la hora de programar consiste en trabajar con archivos, ya sea para almacenar textos, datos ordenados, información de actividades, etc.

Para ello se requieren entender las siguientes operaciones y definiciones:

- ruta de un archivo 
- abrir un archivo
- editar/modificar/copiar un archivo
- eliminar un archivo
- crear un directorio
- eliminar un directorio
- cerrar un archivo


## Ruta de un archivo

Existen dos formas para poder acceder a un archivo, pudiendo ser necesario utilizar una ruta considerando *absolutamente* todos los elementos que describen los elementos o solo describir los elementos *relativos* a la ubicación del programa que se esta ejecutando. 

### Ruta Absoluta

La ruta absoluta es aquella que requiere la descripción completa de la ubicación de un archivo o directorio en el sistema operativo.

=== "Windows"
    ```python
    "C:/Users/$USER/Documents/docs/file.txt"

    ```
=== "Linux"
    ```python
    "/home/$USER/Documents/docs/file.txt"

    ```


### Ruta Relativa

La ruta absoluta es aquella que no requiere la descripción completa de la ubicación de un archivo o directorio, si no que solo requiere el detalle *desde* donde se ubica el programa.

En el caso de que tengas el archivo en el mismo directorio del programa la ruta serian las siguiente.


=== "Windows y Linux"
    ```python
    "file.txt"

    ```

En el caso de que tengas el archivo en una carpeta en el mismo directorio del programa la ruta serian las siguiente.


=== "Windows y Linux"
    ```python
    "archivos/file.txt"

    ```
## Abrir un archivo

En python existe la función ```open()```, la cual permite abrir un archivo tomando una ````ruta``` como parametro de entrada y luego la funcion retorna un **objeto** de tipo ```file```.

```python
file1 = open("text.txt")                                #Ruta relativa
file2 = open("/home/$USER/Documents/docs/file.txt")     #Ruta absoluta

```

Ademas es posible abrir un archivo con opciónes que permiten especificar que es lo que se quiere hacer con un archivo.

|Modo| descripción |
|-----|-----|
|r|(**default**) Abre un archivo para lectura|
|w|Abre un archivo para escribir en el, crear uno si no existe o reemplaza un archivo si ya existe|
|x|Crea un archivo, si el archivo ya existe la operación falla|
|a|Abre un archivo para poder añadir información al final del archivo sin reemplazarlo y crea un archivo en caso de que no exista|
|t|(**default**) Abre el archivo en modo de texto|
|b|Abre el archivo en modo binario|
|+|Abre un archivo para actualizarlo (lectura y escritura)|


```python
file1 = open("text.txt")        # equivalente a "r" o "rt"
file2 = open("text.txt","w")    # escritura en modo de texto
file3 = open("text.txt", "r+b") # escritura y lectura en modo binario

```

## Cerrar un archivo

Al momento de finalizar las operaciones con un archivo determinado, es necesario cerrarlo de modo que se liberen recursos para el sistema.

```python

f.close()
```

Por varios motivos la operación ```open()``` podria fallar (archivo eliminado, falta de permisos, problemas con la memoria, etc.) por lo quees recomendable que se ejecute dentro de un bloque try-except-finally

```python
try:
   f = open("text.txt", encoding = 'utf-8')
   # operaciones
   #...

finally:
   f.close()

```
Otra forma de cerrar de forma segura un archivo es con el operador ```with```. de modo que al terminar las operaciones dentro del bloque, el archivo es cerrado automaticamente.

```python

with open("text.txt",'a') as f:
   # operaciones
```

## Escribir en un archivo

Para escribir en u archivo en un modo compatible. ```w``` para escritura/sobreescritura, ```a``` para añadir, ```x``` para creación y escritura.
El método para escribir en el archivo es ```f.write()``` y este retorna la cantidad de caracteres añadidos al archivo.

```python

with open("text.txt",'w') as f:
    f.write("primera linia\n") # Los saltos de linea no son añadidos de forma automatica
    f.write("penultima linea\n")
    f.write("ultima y final\n") 

```

## Leer un archivo

Para leer un archivo solo es necesario tenerlo previamente abierto en un modo compatible con esta operación y utilizar algun modo que permita leer el archivo.

### f.read()

Esta funcion permite leer un archivo por partes o su totalidad.

```python

with open("text.txt",'r') as f:
   print(f.read(4)) # lee solo 4 caracteres
   print(f.read(5)) # lee los siguientes 5 caracteres
   print(f.read())  # lee el resto de los caracteres
   print(f.read())  # al no quedar más archivo que leer no se mostraria nada

```
### f.tell()
Esta funcion retorna la posición del cursor el que corresponderia al ultimo *byte* leido. 


```python

print(f.tell()) 

```

### f.seek()
Esta funcion permite ajustar a una posición especifica el cursor que lee el archivo.


```python

f.seek(0) #deja el cursor en el inicio del archivo 

```

### leer lineas

#### f.readline()

Mediante este método lee hasta un salto de linea utilizando el metodo del curson de ```f.reed()```. 

```python
print(f.readline())     # 'primera linea\n'
print(f.readline())     # 'penultima linea\n'
print(f.readline())     # 'ultima y final\n'
print(f.readline())     # 
```

#### f.readlines()

Este método retorna en un objeto ```list``` las filas restantes del archivo. En caso de que no queden filas o que el archivo este vacio, simeplemente se retorna una lista vacia.

```python

f.readlines() #['primera linea\n', 'penultima linea\n', 'ultima y final\n']
```

#### for-loop

Mediante un ciclo ```for``` es posible leer mediante lineas.

```python
f = open("text.txt")
for line in f:
    print(line, end = '')
```


## Métodos de file 

|Metodos|		Descripcion|
|--|--|			
|close()    |     Cierra un archivo|
|detach()     |   Separa el buffer binario del archivo y lo retorna.|
|fileno()    |    Retorna el numero del archivo (file descriptor).|
|flush()       |  Refresca el buffer del archivo.|
|isatty()     |   Retorna ```TRUE`` si el archivo es interactivo.|
|read(n)      |   Lee como máximo la cantudad *n* de caracteres hasta el final del archivo.|
|readable()   |   Retorna ```TRUE`` si el archivo permite ser leido.|
|readline(n=-1) | Retorna una linea del archivo, ademas de poder limitar la cantidad de caracteres si se ingresa el parametro numerico.|
|readlines(n=-1) | Retorna un lista que contiene las lineas restantes en el archivo, ademas de poder limitar la cantidad de caracteres si se ingresa el parametro numerico. |
|seek(offset,from=SEEK_SET)|  Cambia la posición del cursor a un numero dado, utilizando como referencia el inicio, la posición actual del cursor o el fin del archivo.|
|seekable()  |     Retorna ```TRUE`` si el archivo permite acceso aleatorio. |
|tell()     |     Retorna la posición del cursor que lee el archivo.|
|truncate(size=None) |    Ajusta el tamaño al numero de bytes dado, en caso de no especificarlo, el archivo es ajustado al mismo tamaño|
|writable()  |    Retorna ```TRUE``` si el archivo puede ser escrito.|
|write(s)     |   Escribe un string de lineas y retorna el numero de caracteres escrito.|
|writelines(lines) |  Escribe una lista de lineas al archivo.|


## Crear un directorio o carpeta

Para crear un directorio no es necesario el objeto file, solo es necesaria ingresar la ruta del directorio que se desea crear.

```python
from os import mkdir
# El parametro corresponde a la ruta del directorio a crear 
mkdir("nuevoDirectorio")                        # Ejemplo con ruta relativa 
mkdir("/home/usuario/documentos/Nueva carpeta") # Ejemplo con ruta absoluta
```

## Eliminar Archivo

Para eliminar un archivo no es necesario el objeto file, solo es necesaria la ruta del archivo.

```python
from os import remove
remove("file.txt")      #Eliminar un archivo
```

```python
from os import rmdir
rmdir("carpeta_vacia")  #Eliminar directorio vacio
```
```python
from shutil import rmtree
rmtree("carpeta")       #Eliminar un directorio con elementos dentro
```