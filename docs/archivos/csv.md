# CSV

CSV es un formato para poder tabular datos de forma ordenada, los datos estan separados por algún caracter, comunmente "," o ";" sin embargo pueden ser otros. teniendo una fila exclusiva para los titulos de las columnas y el resto de las filas contiene los datos.

```csv
nombre,ciudad,mes_cumpleano
Juan Lopez, Temuco, marzo
Andrea Bravo, Villarrica, abril
Belen Candia, Santiago, diciembre

```

En python es necesario importar la libreria ```csv```. y en conjunto con los métodos para poder abrir un archivo se pueden crear, eliminar, modifica o leer uno de estos archivos.

```python

import csv

with open('cumpleaños.txt', mode='r') as csv_file:
    csv_reader = csv.DictReader(csv_file)
    line_count = 0
    for row in csv_reader:
        if line_count == 0:
            print(f'Column names are {", ".join(row)}')
            line_count += 1
        print(f'\t{row["nombre"]} vive en {row["ciudad"]} y nacio el mes de {row["mes_cumpleano"]}.')
        line_count += 1
    print(f'Processed {line_count} lines.')

```