# JSON

Json corresponde a un formato de intercambio de información liviano, ademas de ser de facil lectura y escritura.

[json](https://www.json.org/json-en.html)

JSON, permite representar objetos, arreglos, numeros, etc.

### Ejemplo

```JSON
{  
    "persona": {  
        "nombre":  "Maria Jose",   
        "edad":   25,   
        "mascotas":  true,
        "numerosSuerte": [4, 32, 56, 21]  
    }  
}  

```

## Libreria

Para no tener que traducir un objeto a formato *JSON* de forma manual, como mínimo es necesario utilizar una libreria que contenga las funciones necesarias para poder serializar y deserializar.
la libreria básica de json para su uso en python es ```json```.


```python
import json
```

## Serializar
El proceso de serialzación corresponde al proceso de transformar un objeto a un elemento tipo *json*.

#### Ejemplo diccionario -> json


```python

import json

diccionario =  {
    "nombre":"Andres",
    "edad":20,
    "ciudad":"Talca"
}

jsonString = json.dumps(diccionario)

print(diccionario)


```
##### salida

```python
{'nombre': 'Andres', 'edad': 20, 'ciudad': 'Talca'}
```




## Deserializar
El proceso de serialzación corresponde al proceso de un elemento tipo ***json*** a transformar un objeto.

#### Ejemplo json -> diccionario

```python
import json

jsonString =  '{ "nombre":"Andres", "edad":20, "ciudad":"Talca"}'

#serialización
diccionario = json.loads(jsonString)

#uso del diccionario
print(diccionario["nombre"])
print(diccionario["edad"])
print(diccionario["ciudad"])

```


##### salida
```python
Andres
20
Talca

```
