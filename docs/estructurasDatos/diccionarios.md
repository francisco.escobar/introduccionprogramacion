# Diccionarios

Los diccionarios en python corresponden a un tipo de estructura de datos que contiene pares de objetos correspondiendo a una llave (key) y su valor (value).

## Declaración

Los diccionarios utilizan ```{}``` para la declaración del diccionario (al igual que los [Set](./set.md)), los elementos estan separados por comas (```,```) y los pares llave-valor se separan con dos puntos (```llave:valor```).

```python

vacia = {}                              # Diccionario vacio 
llaveString = {'a':2, 'b':34, 'c':22}   # Diccionario con llave String
llaveEntera = {1:21, 2:'Santiago'}      # Diccionario con llave entera
llaveEntera = {1:2, 'b':4}              # Diccionario con llave mixta



```

## Propiedades de una diccionario

- Los diccionarios son dinamicos
- Los elementos del diccionario pueden ser accedidos por su llave
- Los diccionarios pueden anidarse (es posible añadir un diccionario como un ```valor```)
- Los elementos pueden componerse por:
    - Keys: pueden ser casi cualquier valor arvitrario. Objetos como listas, diccionarios o algun otro tipo de elemento mutable no puede utilizarse como key. 
    - Values: pueden ser casi cualquier valor arvitrario.
- Las llaves deben ser unicas
- Los valores pueden estar duplicados
- El tipo de una lista es ```dict```

## Manipulación de un Diccionario

### Crear una Diccionario con un una funcion

Es posible crear una lista con el método ```dict(iter)``` pasando como párametro elementos en pares ````key,value``` o ```key=value```

#### key=value
=== "Código"
    ```python
    s = dict(a=42, b=3.14, c=7)
    print(s)

    ```
=== "Salida"
    ```python
    {'a': 42, 'b': 3.14, 'c': 7}

    ```

#### key,value
=== "Código"
    ```python
    s = dict([(1, 5), (3, 2)]) 
    print(s)

    ```
=== "Salida"
    ```python
    {1: 5, 3: 2}

    ```

### Acceso al elemento *k-Esimo*

El acceso a un elemento es posible realizarlo segun su llave (```key```) definido.

**llave correcta**
=== "Código"
    ```python
    s = dict([('a', 5), (3, 2)])
    print(s[3])

    ```
=== "Salida"
    ```python
    5

    ```

**llave no existente**
=== "Código"
    ```python
    s = dict([('a', 5), (3, 2)])
    print(s[2])

    ```
=== "Salida"
    ```python
    Traceback (most recent call last):
    File "C:\Users\pacho\PycharmProjects\pythonProject1\main.py", line 2, in <module>
        print(s[2])
    KeyError: 2

    ```

## Funciones que pueden utilizarse con Diccionarios

#### type(s)

Retorna el tipo de dato asociado el objeto.

=== "Código"
    ```python
    s = dict([('a', 5), (3, 2)])
    print(type(s))

    ```
=== "Salida"
    ```python
    <class 'dict'>

    ```

#### len(s)

Retorna el largo del objeto.
=== "Código"
    ```python
    s = dict([('a', 5), (3, 9)])
    print(len(s))

    ```
=== "Salida"
    ```python
    2

    ```


#### sorted(s)

Retorna una lista con las llaves del diccionario ordenadas.

=== "Código"
    ```python
    s = dict([('c', 5), ('b', 2)])
    t = sorted(s)
    print(t)

    ```
=== "Salida"
    ```python
    ['b', 'c']

    ```

#### in

Retorna ```True``` si se encuentra una llave en el diccionario.

=== "Código"
    ```python
    s = dict([('a', 5), ('b', 2)])
    print( 'a' in s)

    ```
=== "Salida"
    ```python
    True

    ```

#### not in

Retorna ```True``` si no se encuentra una llave en el diccionario.
=== "Código"
    ```python
    s = dict([('a', 5), ('b', 2)])
    print('a' not in s)

    ```
=== "Salida"
    ```python
    False

    ```

#### del

Elimina un elemento según la llave.

=== "Código"
    ```python
    s = dict([('a', 5), ('b', 2)])
    del s['a']
    print(s)


    ```
=== "Salida"
    ```python
    {'b': 2}

    ```


## Metodos de Diccionario

- ```clear()```      Elimina todos los elementos de un diccionario
- ```copy()```       Retorna una copia de un diccionario
- ```fromkeys()```   Retorna un diccionario con valores especificos para las llaves y sus valores
- ```get()```        Retorna el valor correspondiente a una llave especifica
- ```items()```      Retorna una lista conteniendo una tupla por cada par llave-valor en el diccionario
- ```keys()```       Retorna una lista con una lista de todas las llaves del diccionario
- ```pop()```        Elimina una item del diccionario dado una llave especifica
- ```popitem()```    Elimina el ultimo par llave-valor insertado
- ```setdefault()``` Retorna el valor de asociado a una llave especifica. Si la llave no existe, inserta la llave con un valor especifico
- ```update()```     Modifica un valor asociado a una llave especifica
- ```values()```     Retorna una lista con todos los valores en un diccionario.


### Ejemplos de uso de algunos métodos de List

#### clear()
#### copy()
#### fromkeys()
#### get()
#### items()
#### keys()
#### pop()
#### popitem()
#### setdefault()
#### update()
#### values()