# Listas

Las listas en python es un tipo de objeto que permite almacenar cualquier tipo de dato dentro de ella.

Puede ser utilizada para modelar datos complejos donde la cantidad y valor de los elementos pueden variar a lo largo del tiempo.

## Declaración

```python

vacia = []
conElementos = [2, 3, 4.8 ,5]
conElementosTipoVarios = [23, "Temuco", 'r']


```

## Propiedades de una Lista

- Las listas mantienen el orden en que han sido definidas
- Pueden componerse por tipos arvitrarios
- Los elementos pueden ser accedidos mediante un indice
- Las listas pueden ser anidadas
- Las listas son mutables
- Las listas son dinamicas
- El tipo de una lista es ```list```

## Manipulación de una Lista

### Crear una lista con un una funcion

Es posible crear una lista con el método ```list(iter)``` pasando como párametro un elemento *iterable*.

=== "Código"
    ```python
    print( list("1234"))

    ```
=== "Salida"
    ```python
    ['1', '2', '3', '4']

    ```

### Acceso al elemento *i-Esimo*

El acceso a un elemento es posible realizarlo segun un indice definido.

**NOTA**: el indice va desde *0* a *largo-1* y de *-1* a *-largo*

**Indice correcto**
=== "Indice"
    ```python
    s = [2, 3, 4.8 ,5, 1]
    print(s[3])

    ```
=== "Salida"
    ```python
    5

    ```

**Indice fuera de rango**
=== "Indice"
    ```python
    s = [2, 3, 4.8 ,5, 1]
    print(s[6])

    ```
=== "Salida"
    ```python
    Traceback (most recent call last):
    File "C:\Users\user\PycharmProjects\pythonProject1\main.py", line 2, in <module>
        print(s[6])
    IndexError: list index out of range

    ```

### Crear una lista a partir de otra

Definiendo el indice de inicio *i* y el fin *j* para obtener una porción de la lista señalada.

=== "Código"
    ```python
    s = [2, 3, 4.8 ,5, 1]
    #s[i:j]
    print( s[1:3])

    ```
=== "Salida"
    ```python
    [3, 4.8]

    ```

### Crear una lista a partir de otra con un paso

Definiendo el indice de inicio *i*, el fin *j* y el paso *k* obtener una porción de la lista señalada.

=== "Código"
    ```python
    s = [2, 3, 4.8 ,5, 1, 13, 15]
    #s[i:j:k]
    print( s[1:6:2])

    ```
=== "Salida"
    ```python
    [3, 5, 13]

    ```

## Funciones que pueden utilizarse con Listas

#### type(s)

Retorna el tipo de dato asociado el objeto.

=== "Código"
    ```python
    s = [9, 3, 5, 1]
    print(type(s))

    ```
=== "Salida"
    ```python
    <class 'list'>

    ```

#### len(s)

Retorna el largo del objeto.
=== "Código"
    ```python
    s = [9, 3, 5, 1]
    print(len(s))

    ```
=== "Salida"
    ```python
    4

    ```

#### min(s)

Retorna el elemento mínimo de la cadena.
=== "Código"
    ```python
    s = [9, 3, 5, 1]
    print(min(s))

    ```
=== "Salida"
    ```python
    1

    ```

#### max(s)

Retorna el elemento máximo de la cadena.
=== "Código"
    ```python
    s = [9, 3, 5, 1]
    print(max(s))

    ```
=== "Salida"
    ```python
    9

    ```

#### in

Retorna ```True``` si una elemento se encuentra en la lista.

=== "Código"
    ```python
    s = [9, 3, 5, 1]
    print( 5 in s)

    ```
=== "Salida"
    ```python
    True

    ```

#### not in

Retorna ```True``` si una elemento no se encuentra en la lista.
=== "Código"
    ```python
    s = [9, 3, 5, 1]
    print( 5 not in s)

    ```
=== "Salida"
    ```python
    False

    ```

#### del

Elimina un elemento segun el indice.

=== "Código"
    ```python
    s = [9, 3, 5, 1]
    del s[1]
    print(s)

    ```
=== "Salida"
    ```python
    [9, 5, 1]

    ```

#### Concatenar \+

Retorna una lista con los nuevos elementos añadidos al final.

=== "Código"
    ```python
    s = [9, 3, 5, 1]
    t = [6, 2]
    print( s + t)

    ```
=== "Salida"
    ```python
    [9, 3, 5, 1, 6, 2]

    ```

#### Concatenar copias \*

Añade *n-1* copias de la lista.

=== "Código"
    ```python
    s = [9, 3, 5, 1]
    print( s * 3)

    ```
=== "Salida"

    ```python
    [9, 3, 5, 1, 9, 3, 5, 1, 9, 3, 5, 1]

    ```
## Metodos de List

- ```l.append( x )```
- ```l.count( x )```
- ```l.extend( s )```
- ```l.index( x )```
- ```l.insert(i, x)```
- ```l.remove( x )```
- ```l.pop(i=-1)```
- ```l.reverse()```
- ```l.sort(cmp=cmp, key=None, reverse=False)```

### Ejemplos de uso de algunos métodos de List

#### l.append( x )

#### l.count( x )

#### l.extend( s )

#### l.index( x )

#### l.insert(i, x)

#### l.remove( x )

