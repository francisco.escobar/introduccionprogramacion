# Sets

En python existen 2 tipos de asociados es este tipo de estructura ```set``` y ```frozenset``. Estos pueden utilizarse para almacenar una coleción de datos y unica de elementos, los items de un "set"  pueden ser distintos pero deben ser "ordenables"(hasheables).

#### Set
Los elementos de un set son mutables y por consecuencia nonecesariamente hasheables.
Los elementos de un set no pueden ser otros set. pero si puede contener frozenset.


#### Frozenset
Los elementos de un frozenset son inmutables y por sonsecuencia deben ser mutables y hasheables.


## Declaración

```python

conElementos = {2, 3, 4.8 ,5}                   # declaración de set con varios elementos
conElementosTipoVarios = {23, "Temuco", 'r'}    # declaración de set con varios elementos de varios tipos
# vacio = {} # esta estructura no corresponde a un set vacio si no que corresponde a un diccionario vacio

```


## Propiedades de un set

- Los set pueden componerse por tipos arvitrarios
- Los elementos pueden ser accedidos mediante un indice
- Las set no pueden ser anidadas
- Las set son mutables
- El tipo de una lista es ```set```

## Manipulación de una set

### Crear una set con un una funcion

Es posible crear una lista con el método ```list(iter)``` pasando como párametro un elemento *iterable*.

=== "Código"
    ```python
    print( set("1234"))

    ```
    
=== "Salida"
    ```python
    {'3', '2', '4', '1'}

    ```

### Acceso al elemento *i-Esimo*

El acceso a un elemento no es posible mediante un indice definido como en el caso de las Listas y Tuplas.


**Indice correcto**
=== "Indice"
    ```python
    s = {2, 3, 4.8 ,5, 1}
    print(s[3])

    ```
=== "Salida"
    ```python
    Traceback (most recent call last):
    File "C:\Users\user\PycharmProjects\pythonProject1\main.py", line 2, in <module>
    print(s[3])
    TypeError: 'set' object is not subscriptable

    ```
