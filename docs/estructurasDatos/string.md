# String o Cadenas de Caracteres 

Los string consisten de secuencias de caracteres, estas cadenas de caracteres pueden ser compuestas por números, letras, incluso caracteres especiales *(*.,+**(['#-_)*.

Los string, comunmente utilizados para poder mostrar mensajes, tambien otros multiples usos que pueden programarse. Para utilizar las cadenas es necesario conocer las operaciones que se pueden realizar con esto.

## Declaración

```python
vacia = ""                                  # cadena vacia
sinPalabras  = ''                           # cadena vacia
conPalabras = "Son las 4 de la tarde?"      # cadena con palabras
nomrbre = "Atlanta"                         # cadena con solo una palabra
especiales = "según dijo ana \'¡que más dia soleado!\'"  #cadena con caracteres especiales

```


## Propiedades de un String

- El tipo de variable asociado a un String  es ```str```.
- Los String son inmutables
- Caracteres solos igual se consideran ```str```

## String como Lista

Es posible operar sobre un String tal como si este fuera una lista cuando se trata con sus indices.

=== "Código"
    ```python
    s = "abcdefg"
    print(s[0])     # imprimir el primer caracter
    print(s[-1])    # imprimir el último caracter
    print(s[0:2])   # imprimir nueva cadena con en los elementos de la cadena original 
                    # desde el indice 0 hasta el indice 2
    
    print(s[2:])    # imprimir nueva cadena con en los elementos de la cadena original
                    # desde el indice 2 hasta el final
    
    print(s[0:5:2]) # imprimir nueva cadena con en los elementos de la cadena original
                    # desde el indice 0 hasta el indice 5, de 2 en 2

    print(s[0::2])  # imprimir nueva cadena con en los elementos de la cadena original
                    # desde el indice 0 hasta el final, de 2 en 2

    ```
=== "Salida"

    ```python
    a
    g
    ab
    cdefg
    ace
    aceg

    ```

## Funciones que pueden utilizarse con String

#### type(s)
Retorna el tipo de dato asociado el objeto.

=== "Código"
    ```python
    s = "34"
    print(type(s))

    ```
=== "Salida"

    ```python
    <class 'str'>

    ```

#### len(s)
Retorna el largo del objeto.
=== "Código"
    ```python
    s = "Carmen"
    print(len(s))

    ```
=== "Salida"

    ```python
    6

    ```

#### Concatenar copias String \*

Retorna una copia del string multiplicado una cantidad definida de veces.

=== "Código"
    ```python
    s = "hola¡"
    print(s*3)

    ```
=== "Salida"

    ```python
    hola¡hola¡hola¡

    ```

#### in

Retorna ```True``` si una cadena se encuentra en otra.

=== "Código"
    ```python
    s = "Hola mundo"
    print("mundo" in s)

    ```
=== "Salida"

    ```python
    True

    ```


## Metodos de String

- ```s.casefold()```
- ```s.capitalize()```
- ```s.center(n,fillchar=' ')```
- ```s.count(sub,start=0,end=sys.maxsize)```
- ```s.decode(encoding='utf-8',errors='strict')```
- ```s.encode(encoding=None,errors='strict')```
- ```s.endswith(suffix,start=0,end=sys.maxsize)```
- ```s.expandtabs(tabsize=8)```
- ```s.find(sub,start=0,end=sys.maxsize)```
- ```s.format(*args,**kwargs)```
- ```s.format_map(mapping)```
- ```s.index(sub,start=0,end=sys.maxsize)```
- ```s.isalnum()```
- ```s.isalpha()```
- ```s.isdecimal()```
- ```s.isdigit()```
- ```s.isidentifier()```
- ```s.islower()```
- ```s.isnumeric()```
- ```s.isprintable()```
- ```s.isspace()```
- ```s.istitle()```
- ```s.isupper()```
- ```s.join(seq)```
- ```s.ljust(n,fillchar=' ')```
- ```s.lower()```
- ```s.lstrip(x=string.whitespace)```
- ```s.replace(old,new,maxsplit=sys.maxsize)```
- ```s.rfind(sub,start=0,end=sys.maxsize)```
- ```s.rindex(sub,start=0,end=sys.maxsize)```
- ```s.rjust(n,fillchar=' ')```
- ```s.rstrip(x=string.whitespace)```
- ```s.split(sep=None,maxsplit=sys.maxsize)```
- ```s.splitlines(keepends=False)```
- ```s.startswith(prefix,start=0,end=sys.maxsize)```
- ```s.strip(x=string.whitespace)```
- ```s.swapcase()```
- ```s.title()```
- ```s.translate(table)```
- ```s.upper()```

### Ejemplos de uso de algunos métodos con String

#### s.capitalize()

Retorna una copia del string cambiando el primer caracter si es una letra por una mayuscula y el resto como minusculas.

=== "Código"
    ```python
    s = "hola mundo"
    print(s.capitalize())

    ```
=== "Salida"

    ```python
    Hola mundo

    ```



#### s.isdigit()

Retorna ```True``` si el String es mayor a 0 y todos los caracteres son digitos.

=== "Código"
    ```python
    s = "34"
    print(s.isdigit())

    ```
=== "Salida"

    ```python
    True

    ```

#### s.islower()
Retorna ```True``` si los caracteres dentro del string son todos minusculas.

=== "Código"
    ```python
    s = "asdf"
    print(s.islower())

    ```
=== "Salida"
    ```python
    True

    ```
#### s.count()
Retorna la cantidad de coincidencias dentro del string.

=== "Código"
    ```python
    s = "Python es muy entretenido de programar"
    print(s.count('o'))

    ```
=== "Salida"

    ```python
    3

    ```

#### s.split()
Retorna un elemento ````list``` con los elementos.

=== "Código"
    ```python
    s = "Python es muy entretenido de programar"
    print(s.split())

    ```
=== "Salida"

    ```python
    ['Python', 'es', 'muy', 'entretenido', 'de', 'programar']

    ```

#### s.find()
Retorna el primer indice donde se encuentre la coincidencia, si no, retorna -1.

=== "Código"
    ```python
    s = "Python es muy entretenido de programar"
    print(s.find('o'))

    ```
=== "Salida"

    ```python
    4

    ```

#### s.replace()
Retorna una copia del string reemplazando todas las ocurrencias del primer elemento por el segundo ingresado.

=== "Código"
    ```python
    s = "Python es muy entretenido de programar"
    print(s.replace('o','a'))

    ```
=== "Salida"

    ```python
    Pythan es muy entretenida de pragramar

    ```

#### s.join()

Retorna una nuevo string que contiene una concatenación de una secuencia de elementos.

=== "Código"
    ```python
    s = "e"
    print(s.join('aiou'))   

    ```
=== "Salida"

    ```python
    aeieoeu

    ```









