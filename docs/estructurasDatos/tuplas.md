# Tuplas

Las tuplas corresponden a una estuctura de datos inmutable, esto quiere decir que no es posible añadir, eliminar o modificar elementos. 

Los elementos dentro de una tupla corresponden a objetos arbitrarios que al igual que en una lista pueden ser de distinto tipo.

## Declaración

```python
t = ()                                      # Declaración de tupla vacia
t = (2, 5, 6, 3)                            # Declaración de tupla con enteros
t = ("Temuco", "Concepción", "Santiago")    # Declaración de tupla con Strings
t = ("Lunes", 2, 3)                         # Declaración de tupla con objetos mixtos
```

## Propiedades de una Tupla

- Las tuplas mantienen el orden en que han sido definidas
- Pueden componerse por tipos arvitrarios
- Los elementos pueden ser accedidos mediante un indice
- Las tuplas son inmutables
- El tipo de una tuplas es ```tuple```

## Manipulación de una Tupla

### Crear una tupla con un una funcion

Es posible crear una lista con el método ```tuple(iter)``` pasando como párametro un elemento *iterable*.

=== "Código"
    ```python
    print( tuple("1234"))

    ```
=== "Salida"
    ```python
    ('1', '2', '3', '4')

    ```
### Acceso al elemento *i-Esimo*

El acceso a un elemento es posible realizarlo segun un indice definido.

**NOTA**: el indice va desde *0* a *largo-1* y de *-1* a *-largo*

**Indice correcto**
=== "Indice"
    ```python
    t = (2, 3, 4.8 ,5, 1)
    print(t[3])

    ```
=== "Salida"
    ```python
    5

    ```

**Indice fuera de rango**
=== "Indice"
    ```python
    t = (2, 3, 4.8 ,5, 1)
    print(t[6])

    ```
=== "Salida"
    ```python
    Traceback (most recent call last):
    File "C:\Users\user\PycharmProjects\pythonProject1\main.py", line 2, in <module>
        print(s[6])
    IndexError: tuple index out of range

    ```


### Crear una tupla a partir de otra.

Definiendo el indice de inicio *i* y el fin *j* para obtener una porción de la tupla señalada.

=== "Código"
    ```python
    s = (2, 3, 4.8 ,5, 1)
    #s[i:j]
    print( s[1:3])

    ```
=== "Salida"
    ```python
    [3, 4.8]

    ```

### Crear una tupla a partir de otra con un paso

Definiendo el indice de inicio *i*, el fin *j* y el paso *k* obtener una porción de la tupla señalada.

=== "Código"
    ```python
    s = [2, 3, 4.8 ,5, 1, 13, 15]
    #s[i:j:k]
    print( s[1:6:2])

    ```
=== "Salida"
    ```python
    [3, 5, 13]

    ```


## Funciones que pueden utilizarse con tuplas

#### type(s)

Retorna el tipo de dato asociado el objeto.

=== "Código"
    ```python
    s = [9, 3, 5, 1]
    print(type(s))

    ```
=== "Salida"
    ```python
    <class 'list'>

    ```

## Funciones que pueden utilizarse con tuplas

#### type(s)

Retorna el tipo de dato asociado el objeto.

=== "Código"
    ```python
    s = (9, 3, 5, 1)
    print(type(s))

    ```
=== "Salida"
    ```python
    <class 'tuple'>

    ```

#### len(s)

Retorna el largo del objeto.
=== "Código"
    ```python
    s = (9, 3, 5, 1)
    print(len(s))

    ```
=== "Salida"
    ```python
    4

    ```

#### min(s)

Retorna el elemento mínimo de la tupla.
=== "Código"
    ```python
    s = (9, 3, 5, 1)
    print(min(s))

    ```
=== "Salida"
    ```python
    1

    ```

#### max(s)

Retorna el elemento máximo de la tupla.
=== "Código"
    ```python
    s = (9, 3, 5, 1)
    print(max(s))

    ```
=== "Salida"
    ```python
    9

    ```

#### in

Retorna ```True``` si una elemento se encuentra en la tupla.

=== "Código"
    ```python
    s = (9, 3, 5, 1)
    print( 5 in s)

    ```
=== "Salida"
    ```python
    True

    ```

#### not in

Retorna ```True``` si una elemento no se encuentra en la tupla.
=== "Código"
    ```python
    s = (9, 3, 5, 1)
    print( 5 not in s)

    ```
=== "Salida"
    ```python
    False

    ```


#### Concatenar \+

Retorna una nueva tupla con los nuevos elementos añadidos al final.

=== "Código"
    ```python
    s = (9, 3, 5, 1)
    t = (6, 2)
    print( s + t)

    ```
=== "Salida"
    ```python
    (9, 3, 5, 1, 6, 2)

    ```

#### Concatenar copias \*

retorna *n-1* copias de la tupla.

=== "Código"
    ```python
    s = (9, 3, 5, 1)
    print( s * 3)

    ```
=== "Salida"

    ```python
    (9, 3, 5, 1, 9, 3, 5, 1, 9, 3, 5, 1)

    ```

## Metodos de tuplas

- ```t.count( x )```
- ```t.index( x )```

### Ejemplos de uso de los métodos de Tupla

#### t.count( x )

=== "Código"
    ```python
    t = (9, 3, 1, 1, 5, 1)
    print(t.count(1))

    ```
=== "Salida"

    ```python
    3

    ```

#### l.index( x )

=== "Código"
    ```python
    t = (9, 3, 5, 1)
    print(t.index(5))

    ```
=== "Salida"

    ```python
    2

    ```


