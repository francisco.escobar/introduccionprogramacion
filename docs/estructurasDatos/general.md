# Estructuras de Datos en Python 

Las estructuras de datos permiten procesar una conjuntos de datos del mismo variados tipos, como una lista de temperaturas registradas durante un día o una lista de nombres obtenida desde un archivo de alumnos de un curso, incluso intercalar datos de tipos distintos en algunos casos.

![Estructuras](./Arreglo.png)

Las estructuras de datos básicas en python son:

- [Listas](./listas.md) ```list```
- [String](./string.md) ```str```
- [Tupla](./tuplas.md) ```tuple```
- [Diccionario](./diccionarios.md) ```dict```
- [Set](./set.md) ```set```


## Propiedades
### Largo

![Largo](./Largo.png)

El largo de una estructura de dato se corresponde a un número mayor o igual a cero, que indica la cantidad de elementos que posee la respectiva estructura.

**Ejemplo 1**
=== "Código"
    ```python
    s = "Carmen"
    print(len(s))

    ```
=== "Salida"

    ```python
    6

    ```
**Ejemplo 2**
=== "Código"
    ```python
    s = {2, 3, 4, 1}
    print(len(s))

    ```
=== "Salida"

    ```python
    4

    ```

### Indice y contenido

![Indice](./indice.png)

Representa la posición de los elementos dentro de la estructura. Estos numeros comienzan en 0 y termina en el Largo-1. Ademas es posible acceder a los elementos mediante indices negativos donde *-1* corresponderia al ultimo elemento y *-largo* corresponderia al primer elemento.

El contenido de la estructura corresponde a los datos almacenados dentro, estos pueden ser de variados tipos y el tamaño podria variar si se introducen nuevos elementos (si la estructura lo permite), modificando el largo, exdentiendo o disminuyendo los indices.

**Nota**: No confundir el inice con el dato almacenado.







