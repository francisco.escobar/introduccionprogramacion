# Bienvenido al Curso de Introducción a la Programación con Python

![portada](portada.jpg)

## Contenidos

- Fundamentos de programación
    -   Conceptos Básicos
    -   PseudoCodigo
    -   Diseño de Argoritmos
    -   Programación con PseInt

- Introducción a Python
    -   Instalación
    -   Syntaxis
    -   Estructuras de control
    -   Estructuras de Datos
    -   Funciones
    -   Programación Orientada a Objetos
    -   Herramientas

