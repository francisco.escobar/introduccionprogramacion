# Operadores Python

Los operadores permiten modificar u obtener nuevos datos a partir de otros.

## Operadores Aritmeticos
 Estos operadores permiten realizar operaciones matematicas básicas.

|Operador| 	Nombre| 	Ejemplo| 
|-----|-----|-----|
|````+````| Suma |	````x + y ````|
|````-````|	Resta |	````x - y ````| 	
|````*````| Multiplicación | ````x * y````| 	
|````/````|	División | ````x / y````| 	
|````%````| Modulo | ````x % y````| 	
|````**````| Potencia |	````x ** y ````|	
|````//````| División entera | ````x // y````|


## Operadores de Asignación
Estos operadores permiten realizar asignaciones a variables.

|Operador |	Ejemplo | Equivalencia |
|------|------|------|
|````=```` 	 |````x = 5 ````| ````	x = 5 	   ```` |
|````+=````  |````x += 3````| ````	x = x + 3 ````	|
|````-=````  |````x -= 3```` |````	x = x - 3 ````	|
|````*= ```` |````x *= 3```` |````	x = x * 3 ````	|
|````/= ```` |````x /= 3```` |````	x = x / 3 ````	|
|````%= ```` |````x %= 3```` |````	x = x % 3 ````	|
|````//= ````|````x //= 3````| 	````x = x // 3 ````	|
|````**= ````|````x **= 3````| 	````x = x ** 3 ````	|

## Operadores de Comparación
Estos operadores son utilizados para comparar dos valores.

|Operador |	Nombre |Ejemplo|
|-----|-----|----| 
|````==````| igual que |````x == y 	````|
|````!=````| distinto que  |````x != y 	````|
|````>````| mayor que |````	x > y 	````|
|````<````| menor que |````	x < y 	````|
|````>=````| menor o igual |````x >= y 	````|
|````<=````| mayor o  igual |```` 	x <= y````|

## Operadores Lógicos
Estos operadores permiten combinar proposiciones condicionales.

|Operador| Nombre	|Descripción| 	Ejemplo|
|----|----|----|---|
|````and````|  y| 	Retorna True si las dos proposiciones son verdaderas | ````x < 5 and  x < 10 	````|
|````or````| o |	Retorna True si una de las proposiciones es verdadera |````x < 5 or x < 4 	````|
|````not````| no |	Invierte el resultado, ej. retorna False Si el resultado es verdadera 	|````not(x < 5 and x < 10)````|