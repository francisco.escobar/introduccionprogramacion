# Instalación de Python 

La ultima versión disponible de python es posible encontrarla en el sitio oficial de [python](https://www.python.org/downloads/)

## Instalación en Windows

1. [Python para Windows](https://www.python.org/downloads/windows/), es recomendable descargar la ultima verisón estable.
2. Ejecutar el archivo de instalación

### Opciones de Instalación

- Seleciona si deseas instalar para todos los usuarios de ese computador.
- Selecionar **Add to PATH**  para poder utilizar python desde **CMD** sin problemas

#### Verificar instalación de Python

- Abrir una consola (powerShell, CMD u otra)
- escribir ```` pyhton ````
- Se deberá mostrar la versión de python instalada.

![install Python](install_Python5.jpg)

#### Verificar Instalación de PIP

- Abrir una consola (powerShell, CMD u otra)
- escribir ```` pip -V ````
- Se deberá mostrar la versión de python instalada.

![install pip](install_Python6.jpg)


## Instalación en MacOS

En los sistemas MacOS, la versión 2.7 de python ya esta preinstalada, sin embargo, lo ideal es instalar la ultima versión. 
Para eso es necesario:

1. [Python para MacOS](https://www.python.org/downloads/macos/), es recomendable descargar la ultima verisón estable.
2. Ejecutar el archivo de instalación. 


## Instalación en Linux

### APT

Ejecuta el siguiente comando en la consola
````bash 
sudo apt-get install python
````

### PACMAN
````bash 
sudo pacman -Syu install python
````


#### Verificar instalación de Python

- Abrir una consola (bash, fish, zsh u otra)
- escribir ```` pyhton ````
- Se deberá mostrar la versión de python instalada.

![install Python](pythonbash.png)

#### Verificar Instalación de PIP

- Abrir una consola (bash, fish, zsh u otra)
- escribir ```` pip -V ````
- Se deberá mostrar la versión de python instalada.

![install pip](pipvbash.png)
