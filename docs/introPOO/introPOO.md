# Clases
## Conceptos basicos

### a. Objeto
El concepto básico de la POO es el de objeto, por lo tanto resulta conveniente desde ya dar una respuesta a la pregunta ¿Qué es un objeto? Inicialmente se puede decir que un objeto, según la Real Academia de la Lengua Española (RAE), es “Todo lo que puede ser materia de conocimiento o sensibilidad de parte del sujeto, incluso este mismo”. Lo anterior no está planteado desde una perspectiva computacional, pues si así fuese, podríamos decir que un objeto es:

- Cualquier cosa, real o abstracta, que posea un estado interno y un conjunto de operaciones o métodos.
- Una colección de operaciones encapsuladas que comparten un estado común.

![objeto](objeto.png)

Representación de un Objeto, conteniendo su nombre, atributos y operaciones.

### b. Estado
De las definiciones técnicas anteriores, se observa un elemento común, este es el de estado, que se define como el valor de los atributos del objeto en un cierto instante.

### c. Comportamiento
Se relaciona con las operaciones y como éstas hacen que se comporte el objeto ante una acción, la cual es provocada por la invocación a un método de la clase.

### d. Atributos
Son los elementos que definen un objeto, son sus “propiedades”. Conocidas en la programación tradicional como las variables. De lo anterior podemos concluir que un atributo debe entonces definirse con un nombre que lo represente y un tipo de dato asociado.

Otro elemento a considerar además del tipo de datos y nombre de cada atributo es lo relativo a su nivel de encapsulamiento.

### e. Métodos
También llamadas operaciones, son los que determinan aquello que se puede hacer con el objeto. Conocidos en la programación tradicional como las funciones o procedimientos. Puede entenderse como un conjunto de funciones pertenecientes a una clase, que actúan y modifican los atributos del objeto.

### f. Clase
Así como un objeto es un concepto con un grado de abstracción mayor que las comunes estructuras de datos, es posible hacer una abstracción aún mayor e introducir el concepto de Clase, entendiéndola como una descripción generalizada que hace referencia a una colección de objetos que poseen las mismas características.

#### Estado de un Objeto y la instanciación de una Clase
Luego, a partir de los conceptos hasta ahora mencionados, se puede decir que un objeto no es sino una instancia de una clase.

- Pensar en un **Objeto** como “caso particular” de una **Clase**.
- Los atributos poseen valores específicos en un instante de tiempo, que lo identifican como un único objeto.
- Lo anterior indica que los objetos son “dinámicos”, pues su estado puede cambiar en el tiempo.


Para nuestro contexto entenderemos la **abstracción** como una aproximación o abordaje del diseño que hace hincapié en los aspectos más importantes de algo, sin preocuparse por los detalles menos importantes.

##### Ejemplo

![objeto](objetoEj.png)

Abstracción de los detalles de los objetos para determinar la clase.

![clase](clase.png)

### g. Encapsulamiento
Esta característica fundamental de la POO permite a los objetos crear una cápsula a su alrededor, convirtiéndose en una “caja negra” para el resto de los objetos, ocultando la información interna que manipula. Lo anterior permite modificar el código interno de alguna(s) operación(es) y si lo hacemos respetando los argumentos definidos para las entradas y las salidas de dicha operación, es posible modificar y optimizarlo en forma constante, sin efectos laterales o secundarios.

La figura ilustra el hecho que gracias al encapsulamiento lo que se busca es proteger tanto como sea posible la integridad de los atributos del objeto y no permitir que estos puedan ser accedidos directamente. De lo anterior es que entonces se definen un conjunto de operaciones con acceso público, que permiten acceder a los atributos del objeto.

![protected](protected.png)

En la figura se intenta explicar una de las ventajas del encapsulamiento, pues el código asociado a la operación Suma(n1, n2) podemos modificarlo de acuerdo a nuestras necesidades y si tenemos cuidado en respetar las entradas y salidas que esta operación usa, el resto de los objetos que usen Suma(n1, n2) no se habrán enterado (ni les interesaría hacerlo) de los cambios o mejoras que realicemos al interior de su código.

![encapsulamiento](encapsulamiento.png)


#### Acceso a los elementos (atributos u operaciones) de una clase

- Acceso público: Cualquier miembro público de una clase es accesible desde cualquier parte donde sea accesible el propio objeto.
- Acceso privado: Los miembros privados de una clase sólo son accesibles por los propios miembros de la clase y en general por objetos de la misma clase, pero no desde funciones externas o desde funciones de clases derivadas.
- Acceso protegido: Con respecto a las funciones externas, es equivalente al acceso privado, pero con respecto a las clases derivadas se comporta como público.

### h. Herencia
Así como una clase representa genéricamente a un grupo de objetos que comparten características comunes, la herencia permite que varias clases compartan aquello que tienen en común y no repetirlo en cada clase. Consiste en propagar atributos y operaciones a través de las subclases definidas a partir de una clase común.

Nos permite crear estructuras jerárquicas de clases donde es posible la creación de subclases, que incluyan nuevas operaciones y atributos que redefinen los objetos. Estas subclases permiten así, crear, modificar o inhabilitar propiedades, aumentando de esta manera la especialización de la nueva clase.

En la figura se muestra un ejemplo del concepto de herencia, donde se tiene una clase llamada Vehículo (superclase), la cual permite representar de forma genérica a cualquier tipo de vehículo, a partir de la cual a su vez pueden originarse tipos especiales de vehículos, en este ejemplo se tienen las subclases: Sedán, Camión y Furgón, cada una de ellas con atributos y operaciones comunes, pero a la vez definen nuevos atributos u operaciones que permiten identificarlo como un tipo particular de vehículo. Entonces es posible decir que un objeto perteneciente a la clase Sedán es también un objeto de tipo Vehículo o también decir que es una especialización de la superclase, dada la relación de herencia entre ambas clases.

![Herencia](herencia.png)

### i. Mensaje

Es el medio a través del cual se comunican los objetos y es la forma en que estos acceden a los servicios de otro objeto. Lo anterior implica que mediante un mensaje, un objeto le pide a otro que ejecute alguna operación que éste posee, per quien la pide no (concepto de delegación de servicios). Un objeto sólo puede mandar mensajes a aquellos objetos que lo “conocen”, estos son aquellos con los que están relacionados.

Para entender de mejor manera el concepto de mensaje, creemos importante que te plantees la siguiente pregunta ¿Cómo un objeto ejecuta un mensaje? Una respuesta a esta pregunta podríamos darla usando el siguiente algoritmo.
```Java
Un objeto recibe un mensaje.
El objeto busca el mensaje en su propia clase.
Si ( el objeto encuentra el mensaje en su clase )
    lo ejecuta
Sino
    Si ( el objeto “sube por la jerarquía de clases a la que pertenece” y encuentra la 1ª clase
    que lo implemente )
        lo ejecuta
    Sino
        devuelve un error.
    Fin Si
Fin Si

```
Una representación de la interacción entre objetos a través de un mensaje se muestra en la figura, donde se puede observar como ambos objetos se encuentran relacionados. El objeto de tipo Trabajador identificado como Juan se encuentra asociado al objeto cA del tipo CajeroAutomatico. El mensaje girar dinero se observa en la flecha desde el objeto Juan, con la intención de realizar un giro hacia el objeto cA. En este caso el objeto Juan desea realizar un giro, él no sabe como, pero si sabe que el objeto cA lo puede hacer, luego el objeto Juan le hace una petición al objeto cA mediante dicho mensaje.

![mensaje](mensaje.png)


## Resumen

![resumen](resumen.png)

## Estructura Básica


```python

class Figura: # clase padre
    def __init__(self, lado1, lado2): # Constructor
        self.lado1 = lado1
        self.lado2 = lado2

    def dibujar(self):  #método
        pass

        

class Cuadrado (Figura):  # clase hija
    def __init__(self, lado):       # Constructor
        super().__init__(lado,lado)
        self.lado = lado

    def agrandar(self): #método
        self.lado += 1
        super().dibujar
    
    def achicar(self):  #método
        self.lado -= 1
        super().dibujar
        
```


## Modificadores de acceso

Los modificadores en lenguajes orientados a objetos (*C++*,*python*,*java*, etc.)  sirven para restringir el acceso a variables y métodos de una clase determinada.

En el caso de python existen tres niveles de acceso:

- ```public```
- ```protected```
- ```private```

### Acceso Publico

El acceso publico, quiere decir que las variables y métodos son accesibles desde cualquier parte de un problema.
En el caso de python, por defecto todo es publico.

```python

#Declaración de clase
class Persona:
    # Constructor
    def __init__(self, nombre, edad):
        # atributos publicos
        self.nombre = nombre
        self.edad = edad

    # método publico
    def cumpleanios(self):
        self.edad+=1

    def mostrar(self):
        print("nombre", self.nombre)
        print("edad", self.edad)


```
##### Salida
```python

p = Persona("Atena", 28)        # Creación  de objeto
p.cumpleanios()                 # acceso al método publico
p.mostrar()                     # acceso al método publico
print(p.nombre)                 # acceso al atributo 

```


### Acceso Protegido

Los elementos protegidos deverian ser accedidos solo desde la clase y sus subclases. Sin embargo, esto solo es indicativo en python

El modificador para acceso protegido es "_" utilizado como prefijo al nombre del atributo/método.


```python

#Declaración de clase
class Persona:
    # atributos protected
    _nombre = None
    _edad = None

    # Constructor
    def __init__(self, nombre, edad):
        # atributos publicos
        self._nombre = nombre
        self._edad = edad

    # método proected
    def _cumpleanios(self):
        self._edad+=1

    # método publico
    def mostrar(self):
        print("nombre", self._nombre)
        print("edad", self._edad)


#Declaración de clase hija
class Estudiante(Persona):

    #constructor
    def __init__(self, nombre, edad, matricula):
        Persona.__init__(self,nombre,edad)
        self.matricula = matricula

    #método publico
    def mostrarDatos(self):
        self.mostrar()
        print("matricula", self.matricula)

```
##### uso

```python
e = accesoProtected.Estudiante("Calisto", 23, "238717762k89")
e.mostrarDatos()
e._cumpleanios() # Acceso a método protected
e.mostrar()

```

##### Salida

```txt
nombre Calisto
edad 23
matricula 238717762k89
nombre Calisto
edad 24

```

### Acceso Privado

Los elementos privados pueden ser accedidos solo desde la clase.

El modificador para acceso privado es "__" utilizado como prefijo al nombre del atributo/método.

```python

#Declaración de clase
class Persona:
    # atributos privados
    __nombre = None
    __edad = None

    # Constructor
    def __init__(self, nombre, edad):
        # atributos publicos
        self.__nombre = nombre
        self.__edad = edad

    # método privado
    def __cumpleanios(self):
        self.__edad+=1

    # método publico
    def mostrar(self):
        print("nombre", self.__nombre)
        print("edad", self.__edad)


#Declaración de clase hija
class Estudiante(Persona):

    #constructor
    def __init__(self, nombre, edad, matricula):
        Persona.__init__(self,nombre,edad)
        self.matricula = matricula

    #método publico
    def mostrarDatos(self):
        self.mostrar()
        print("matricula", self.matricula)

```

##### uso

```python
e = accesoProtected.Estudiante("Calisto", 23, "238717762k89")
e.mostrarDatos()
e.__cumpleanios() # Acceso a método privado
e.mostrar()

```

##### Salida

```txt
nombre Calisto
edad 23
matricula 238717762k89
Traceback (most recent call last):
  File "C:\Users\user\PycharmProjects\pythonProject1\uso.py", line 3, in <module>
    e.__cumpleanios() # Acceso a método privado
AttributeError: 'Estudiante' object has no attribute '__cumpleanios'

```