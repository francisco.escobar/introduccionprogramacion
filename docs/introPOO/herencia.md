# Herencia

Así como una clase representa genéricamente a un grupo de objetos que comparten características comunes, la herencia permite que varias clases compartan aquello que tienen en común y no repetirlo en cada clase. Consiste en propagar atributos y operaciones a través de las subclases definidas a partir de una clase común.

Nos permite crear estructuras jerárquicas de clases donde es posible la creación de subclases, que incluyan nuevas operaciones y atributos que redefinen los objetos. Estas subclases permiten así, crear, modificar o inhabilitar propiedades, aumentando de esta manera la especialización de la nueva clase.

En la figura se muestra un ejemplo del concepto de herencia, donde se tiene una clase llamada Vehículo (superclase), la cual permite representar de forma genérica a cualquier tipo de vehículo, a partir de la cual a su vez pueden originarse tipos especiales de vehículos, en este ejemplo se tienen las subclases: Sedán, Camión y Furgón, cada una de ellas con atributos y operaciones comunes, pero a la vez definen nuevos atributos u operaciones que permiten identificarlo como un tipo particular de vehículo. Entonces es posible decir que un objeto perteneciente a la clase Sedán es también un objeto de tipo Vehículo o también decir que es una especialización de la superclase, dada la relación de herencia entre ambas clases.

![Herencia](herencia.png)


## Herencia - Generalización

La herencia es una relación entre clases, en la que una clase comparte la estructura y/o comportamiento definidos en una o más clases. (Booch, 1994)

La herencia entre clases consiste en una relación especial que conecta dos o más conceptos muy similares entre sí, dentro de un problema.

Permite que una clase tome todos los atributos y métodos de otra clase, esto evita tener que redefinir atributos y métodos similares en varias clases parecidas.


### Generalización UML

- La clase de la izquierda es llamada clase *hija*, subclase o clase derivada
- La clase de la derecha es llamada clase *padre*, superclase o clase base



=== "Diagrama"

    ![herenciaejemplo](generalizacionEjemplo.png)

=== "Clase Animal"

    ```python
    class Animal:
        __numPatas = None
        __especie = None

        #...

        mostraNumPatas():
            pass
            #...

    ```

=== "Clase Gato"

    ```python
    class Gato (Animal):
        __nombre = None
        #...

        public void morder():
            pass
            #...
        
        public void escalar():
            pass
            #...
        
    ```



NOTA: python permite herencia multiple entre clases
